## ArtifactUprising Shopping Cart Exercise
by [Chad Steele](http://chadsteele.com)

[Play with it online here](https://artifactuprising-chadsteele.netlify.com/)

This project leverages 3rd party code and data from [Algolia](https://www.algolia.com/doc/guides/building-search-ui/resources/demos/react/#e-commerce) to provide a rich shopping experience.  You can see it without a shopping cart [here](https://react-instantsearch.netlify.com/examples/e-commerce/search/)

I added the minimal code necessary to satisfy the shopping cart exercise
  * See the [diff here](https://bitbucket.org/zippyskippy/au-app/commits/ea193c70ece39681ccf64314ad94a454dac1479d)
  * the crux of the code was developed in less than 4 hours here [ShoppingCart.js](https://bitbucket.org/zippyskippy/au-app/commitsea193c70ece39681ccf64314ad94a454dac1479d#chg-src/ShoppingCart.js)
  * and the TDD is here [ShoppingCart.test.js](https://bitbucket.org/zippyskippy/au-app/commits/ea193c70ece39681ccf64314ad94a454dac1479d#chg-src/ShoppingCart.test.js)
  * I reused a personal library that I developed last year and reuse it on many projects [EventManager.js](https://dev.to/chadsteele/eventmanager-an-agnostic-alternative-to-redux-2m3d)
  * I developed and [dockerized mock data server api here](https://bitbucket.org/zippyskippy/artifactuprising/src/master/api/).  It's not very interesting.  I did not modify the Algolia software to use it though, but that would be a possible next step if I had more time and it was required. 

## UI/UX enhancements
I added the following UI elements to the Algolia code
  * a sticky ShoppingCart icon/indicator in the upper right of the page
  * you can click on it to scroll down to the shopping cart contents section of the SPA
  * I added a [buy] button to every item in the catalog.  If you click it more than once it updates the count instead of adding a lineitem
  * I added the shopping cart summary to the bottom of the page that sums up the total items and total cost, plus it lets you adjust the quantity of each item and delete it from the order

![ShoppingCart](ShoppingCart.png)

## My Analysis 
given your requirements

Subjective Goals
  * Write as little code as possible
  * Leverage 3rd parties and open source
  * Do not invest in style
  * Dazzle my employer

Objectives
  * Complete in less than 4 hours

User Stories
  * As a user, I want to see an empty cart when I first visit the app, so that I’m inspired to buy
  * As a user, I want to add items to my shopping cart, so that I can purchase later
  * As a user, I want to see the contents of my cart upon adding an item to that cart
  * As a user, I want to be able to edit the quantity of items per item in my cart
  * As a user, I want to see a summary of total items and total amount on my shopping cart page

As a developer, 
  * I must implement the app in separate docker containers
  * The app must be easy to start, test, and understand
  * The app must have both a server and client side component

Notes:
  * This should be a very basic cart implementation. Mock data as you see fit
  * We are not looking for something visually appealing, don’t spend too much time on the CSS side of things
  * It’s okay to cut corners and “hardcode” things, just be prepared to answer questions about this in a follow up discussion
  * Notice: there’s no requirement to submit the order or to authenticate the user.

Strategies

There appear to be the following objects:
  * Catalog of items
  * An item
  * A shopping cart

Other objects that won’t be implemented yet:
  * User
  * Order

Server side api
  * No authentication required; and so, without authentication, there’s no point in retaining the shopping cart on the server, therefore, I can do it in the client.
  * Purchase is not required 
  * The only server side requirement at this point is to provide the inventory.
  * I imagine a future api for submitting orders, but that’s not required now.
  * I’ll leverage a 3rd party catalog  * probably algolia sample data
  * Catalog.Search()

Client side 
  * Maintain a cart and the summary:
  * ShoppingCart.add(item)
  * ShoppingCart.updateQuantity(item, n)
  * ShoppingCart.getItems()
  * ShoppingCart.getSummary()
  * ShoppingCart.submitOrder()  * todo

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

