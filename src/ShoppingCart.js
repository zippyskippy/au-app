/*
For simplicity, I plan to put almost all of my custom code in this one file.  
The goal is to add a [buy] button and shopping cart contents to the 3rd party ecommerce example

ShoppingCart.add(item)
ShoppingCart.updateQuantity(item, n)
ShoppingCart.getItems()
ShoppingCart.getSummary()


*/

import React from 'react';
import { useState } from 'react';
import './ShoppingCart.css'
import events, { EventHandler } from './EventManager'


export class ShoppingCart {

    constructor() {
        this.lineitems = [];
    }


    /* 
    Called by the [buy] button
    */
    add(item, n) {
        if (!item) return;
        n = n || 1;
        var lineitem = this.lineitems.find(it => it.item.objectID === item.objectID);
        if (lineitem) {
            //found, update the count
            lineitem.count += n;
        } else {
            //not found, add it to the list
            this.lineitems.push({ count: n, item: item });
        }
    }

    delete(item) {
        if (!item) return;
        //remove item from list
        this.lineitems = this.lineitems.filter(it => it.item.objectID !== item.objectID);
    }

    /*
    Called when updating the quantity in the summary
    */
    updateQuantity(item, n) {
        if (!item) return;
        if (n === 0) {
            this.delete(item);
        } else {
            n = n || 1;
            var lineitem = this.lineitems.find(it => it.item.objectID === item.objectID);
            if (lineitem) {
                //found, update the count
                lineitem.count = n;
            } else {
                throw new Error({ function: "updateQuantity", msg: "missing item", item: item });
            }
        }
    }

    getLineItems() {
        return this.lineitems;
    }

    getSummary() {
        return {
            count: this.lineitems.reduce((total, it) => total + it.count, 0),
            cost: this.lineitems.reduce((total, it) => total + it.item.price * it.count, 0)
        }
    }

}


const myCart = new ShoppingCart();


export function BuyButton(props) {

    return <div className="buybutton" style={{ backgroundColor: '#f1bb4e' }}
        onClick={e => {
            myCart.add(props.item);
            events.dispatch("update shopping cart");
        }}
    >
        buy
        </div>
}

export function ShoppingCartButton(props) {

    const [count, setCount] = useState(myCart.getSummary().count);

    return <div className="shoppingcart"
        onClick={e => {
            window.scrollTo(0, document.body.scrollHeight);
        }}
    >
        <EventHandler event="update shopping cart" handler={e => {
            setCount(myCart.getSummary().count);
        }} />

        {/* //https://material.io/resources/icons/?search=shopping&icon=shopping_cart&style=outline */}
        <svg xmlns="http://www.w3.org/2000/svg"
            height="24"
            viewBox="0 0 24 24"
            width="24">
            <path d="M0 0h24v24H0V0z" fill="none" />
            <path d="M15.55 13c.75 0 1.41-.41 1.75-1.03l3.58-6.49c.37-.66-.11-1.48-.87-1.48H5.21l-.94-2H1v2h2l3.6 7.59-1.35 2.44C4.52 15.37 5.48 17 7 17h12v-2H7l1.1-2h7.45zM6.16 6h12.15l-2.76 5H8.53L6.16 6zM7 18c-1.1 0-1.99.9-1.99 2S5.9 22 7 22s2-.9 2-2-.9-2-2-2zm10 0c-1.1 0-1.99.9-1.99 2s.89 2 1.99 2 2-.9 2-2-.9-2-2-2z" />
        </svg>
        <div className="count">{count} items</div>
    </div>
}

function QuantityControl(props) {
    const [val, setValue] = useState(props.lineitem.count);

    return <>

        <input type="number" value={val} min={1} style={{ width: '40px' }}
            onChange={e => {
                var val = parseInt(e.target.value);
                setValue(val);
                myCart.updateQuantity(props.lineitem.item, val);
                events.dispatch("update shopping cart");
            }}
        />

    </>
}

function ShoppingCartContents(props) {
    const [items, setItems] = useState([]);
    const [summary, setSummary] = useState({});

    return <div >
        <EventHandler event="update shopping cart" handler={e => {
            setItems([...myCart.getLineItems()]);
            setSummary(myCart.getSummary());
        }} />

        <h2>Shopping Cart Contents</h2>
        <div>Total Items: {summary?.count || 0} |   Total Cost: ${parseFloat(summary?.cost || 0).toFixed(2)} </div>
        <hr />
        {
            items.map(itm => {
                return (<div key={itm.item.objectID + 'x' + itm.count}>
                    <QuantityControl lineitem={itm} /> x {itm.item.name}  ${itm.item.price}
                    <span style={{ marginLeft: '10px', fontSize: '10px', color: 'red', cursor: "pointer" }} onClick={e => {
                        myCart.delete(itm.item);
                        events.dispatch("update shopping cart");
                    }}>remove</span>
                </div>)
            })
        }
    </div>

}

export default ShoppingCartContents;